---@diagnostic disable: undefined-global
local key = vim.api.nvim_set_keymap

--key(
--"n",
--"<leader>/",
--":lua require('Comment.api').toggle.linewise.current(nil, {cfg})<CR>",
--{ noremap, desc = "Comments line" }
--)

key("n", "<leader>b", ":lua require'dap'.toggle_breakpoint()<CR>", { noremap, desc = "Creates breakpoint" })
key("n", "<leader>l", ":lua require'dap'.continue()<CR>", { noremap, desc = "DAP continue" })
key("n", "<leader>o", ":lua require'dap'.step_over()<CR>", { noremap, desc = "DAP step over" })
key("n", "<leader>i", ":lua require'dap'.step_into()<CR>", { noremap, desc = "DAP step into" })

key("n", "<leader>e", ":NnnExplorer<CR>", { noremap, desc = "Opens nnn" })

key("n", "<leader>t", ":TagbarToggle<CR>", { noremap, desc = "Opens tagbar" })

key("n", "<leader>g", ":LazyGit<CR>", { noremap, desc = "Opens Lazygit" })
key("n", "<leader>q", ":CodeActionMenu<CR>", { noremap, desc = "Opens quick actions" })
key("n", "<leader>u", ":bnext<CR>", { noremap, desc = "Cycles buffers" })

--key("n", "<leader>p", ":ProjectMgr<CR>", { noremap, desc = "Opens project manager" })

vim.keymap.set("n", "<leader>rn", function()
	return ":IncRename " .. vim.fn.expand("<cword>")
end, { expr = true, noremap, desc = "Renames variable" })

vim.keymap.set("n", "<A-h>", require("smart-splits").resize_left)
vim.keymap.set("n", "<A-j>", require("smart-splits").resize_down)
vim.keymap.set("n", "<A-k>", require("smart-splits").resize_up)
vim.keymap.set("n", "<A-l>", require("smart-splits").resize_right)
-- moving between splits
vim.keymap.set("n", "<C-h>", require("smart-splits").move_cursor_left)
vim.keymap.set("n", "<C-j>", require("smart-splits").move_cursor_down)
vim.keymap.set("n", "<C-k>", require("smart-splits").move_cursor_up)
vim.keymap.set("n", "<C-l>", require("smart-splits").move_cursor_right)

key("n", "<leader>h", ":HopWord<CR>", { noremap, desc = "Hops to word" })

vim.keymap.set("n", "K", require("hover").hover, { desc = "Opens hover menu" })
key("n", "<leader>cb", ":CBccbox<CR>", { noremap, desc = "Creates a box around the comment" })
vim.api.nvim_set_keymap(
	"n",
	"n",
	[[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]],
	{ noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
	"n",
	"N",
	[[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]],
	{ noremap = true, silent = true }
)

key("n", "<leader>ce", ":FeMaco<CR>", { noremap = true, desc = "Markdown code block lsp" })

key("n", "<leader>ff", ":Telescope frecency<CR>", { noremap = true, desc = "Telescope find files" })

vim.keymap.set("n", "s", function()
	require("query-secretary").query_window_initiate()
end, {})
