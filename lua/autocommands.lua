local autocmd = vim.api.nvim_create_autocmd
local autogroup = vim.api.nvim_create_augroup

local Custom = autogroup("Git", { clear = true })

autocmd({ "BufEnter", "BufWinEnter" }, {
	pattern = { "*.cpp", "*.c", "*.h" },
	callback = function()
		vim.cmd([[compiler gcc]])
		vim.cmd([[set makeprg=make]])
	end,
	group = Custom,
})

autocmd({ "BufEnter", "BufWinEnter" }, {
	pattern = { "*.md", "*.txt" },
	callback = function()
		vim.opt.spell = true
		vim.opt.spelllang = { "en_us" }
		require("cmp").setup.buffer({ enabled = false })
	end,
	group = Custom,
})

autocmd({ "BufDelete", "BufLeave" }, {
	pattern = { "*.md", "*.txt" },
	callback = function()
		require("cmp").setup.buffer({ enabled = true })
	end,
	group = Custom,
})

autocmd("User", {
	pattern = "GitConflictDetected",
	callback = function()
		vim.notify("Conflict detected in " .. vim.fn.expand("<afile>"))
		vim.keymap.set("n", "cww", function()
			engage.conflict_buster()
			create_buffer_local_mappings()
		end)
	end,
})

autocmd("BufWritePre", {
	buffer = bufnr,
	callback = function()
		vim.lsp.buf.format({ bufnr = bufnr })
	end,
})

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
require("null-ls").setup({
	-- you can reuse a shared lspconfig on_attach callback here
	on_attach = function(client, bufnr)
		if client.supports_method("textDocument/formatting") then
			vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
			vim.api.nvim_create_autocmd("BufWritePre", {
				group = augroup,
				buffer = bufnr,
				callback = function()
					vim.lsp.buf.format({ bufnr = bufnr })
				end,
			})
		end
	end,
})
