local luasnip = require("luasnip")
local cmp = require("cmp")

local has_words_before = function()
	local line, col = unpack(vim.api.nvim_win_get_cursor(0))
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end


cmp.setup({
	window = {
		completion = {
			winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None",
			col_offset = -3,
			side_padding = 0,
		},
	},
	formatting = {
		fields = { "kind", "abbr", "menu" },
		format = function(entry, vim_item)
			local kind = require("lspkind").cmp_format({ mode = "symbol_text", maxwidth = 50 })(entry, vim_item)
			local strings = vim.split(kind.kind, "%s", { trimempty = true })
			kind.kind = " " .. strings[1] .. " "
			kind.menu = "    (" .. strings[2] .. ")"

			return kind
		end,
	},
	mapping = cmp.mapping.preset.insert({
		["<C-b>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-Space>"] = cmp.mapping.complete(),
		["<C-e>"] = cmp.mapping.abort(),
		["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.

		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),

		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}),

	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},

	cmp.setup({
		sources = cmp.config.sources({
			{ name = "nvim_lsp" },
			{ name = "buffer" },
			{ name = "nvim_lsp_signature_help" },
			{ name = "spell" },
			{ name = "path" },
			{ name = "nvim_lua" },
			{ name = "luasnip" },
			{ name = "buffer-lines" },
		}),
	}),

	cmp.setup.cmdline(":", {
		sources = {
			{ name = "cmdline" },
			{ name = "path" },
		},
	}),

	cmp.setup.cmdline("/", {
		sources = {
			{ name = "buffer" },
			{ name = "cmdline_history" },
		},
	}),

	cmp.setup.filetype({ "dap-repl", "dapui_watches" }, {
		sources = {
			{ name = "dap" },
		},
	}),
})

vim.cmd([[highlight CmpItemAbbrDeprecated guibg=NONE gui=strikethrough guifg=#808080]])
vim.cmd([[highlight CmpItemAbbrMatch guibg=NONE guifg=#569CD6]])
vim.cmd([[highlight CmpItemAbbrMatchFuzzy guibg=NONE guifg=#569CD6]])
vim.cmd([[highlight CmpItemKindVariable guibg=NONE guifg=#9CDCFE]])
vim.cmd([[highlight CmpItemKindInterface guibg=NONE guifg=#9CDCFE]])
vim.cmd([[highlight CmpItemKindText guibg=NONE guifg=#9CDCFE]])
vim.cmd([[highlight CmpItemKindFunction guibg=NONE guifg=#C586C0]])
vim.cmd([[highlight CmpItemKindKeyword guibg=NONE guifg=#D4D4D4]])
vim.cmd([[highlight CmpItemKindProperty guibg=NONE guifg=#D4D4D4]])
vim.cmd([[highlight CmpItemKindUnit guibg=NONE guifg=#D4D4D4]])
