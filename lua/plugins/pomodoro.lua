require("pomodoro").setup({
	time_work = 10,
	time_break_short = 1,
	time_break_long = 5,
	timers_to_long_break = 6,
})
