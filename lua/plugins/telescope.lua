require("telescope").setup({
	defaults = {
		-- Default configuration for telescope goes here:
		-- config_key = value,
		mappings = {
			i = {
				-- map actions.which_key to <C-h> (default: <C-/>)
				-- actions.which_key shows the mappings for your picker,
				-- e.g. git_{create, delete, ...}_branch for the git_branches picker
				["<C-h>"] = "which_key",
			},
		},
	},
	pickers = {
		-- Default configuration for builtin pickers goes here:
		-- picker_name = {
		--   picker_config_key = value,
		--   ...
		-- }
		-- Now the picker_config_key will be applied every time you call this
		-- builtin picker
	},
	extensions = {
		["zf-native"] = {
			-- options for sorting file-like items
			file = {
				-- override default telescope file sorter
				enable = true,
				-- highlight matching text in results
				highlight_results = true,
				-- enable zf filename match priority
				match_filename = true,
				-- options for sorting all other items
				generic = {
					-- override default telescope generic item sorter
					enable = true,
					-- highlight matching text in results
					highlight_results = true,
					-- disable zf filename match priority
					match_filename = false,
				},
			}
		},
	}
})

require("telescope").load_extension("projects")
require("telescope").load_extension("frecency")
require("telescope").load_extension("zf-native")
require("telescope").load_extension("packer")
