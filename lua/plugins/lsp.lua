local servers = {
	"clangd",
	"sumneko_lua",
	"marksman",
	"bashls",
	"cmake",
	"pyright",
	"rust_analyzer",
	"jsonls",
	"html",
	"tailwindcss",
	"tsserver",
	"intelephense",
	"texlab",
}

require("mason").setup()
require("mason-lspconfig").setup()

require("luasnip.loaders.from_vscode").lazy_load()

vim.cmd([[autocmd! ColorScheme * highlight NormalFloat guibg=#2e3440]])
vim.cmd([[autocmd! ColorScheme * highlight FloatBorder guifg=white guibg=#2e3440]])

local border = {
	{ "🭽", "FloatBorder" },
	{ "▔", "FloatBorder" },
	{ "🭾", "FloatBorder" },
	{ "▕", "FloatBorder" },
	{ "🭿", "FloatBorder" },
	{ "▁", "FloatBorder" },
	{ "🭼", "FloatBorder" },
	{ "▏", "FloatBorder" },
}

local handlers = {
	["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = border }),
	["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = border }),
}

--require("lsp_signature").setup()

local lspkind = require("lspkind")

local lspconfig = require("lspconfig")

local navic = require("nvim-navic")

for _, lsp in ipairs(servers) do
	lspconfig[lsp].setup({
		capabilities = require("cmp_nvim_lsp").default_capabilities(),
		on_attach = function(client, bufnr)
			navic.attach(client, bufnr)
		end,
		handlers = handlers,
	})
end

lspconfig.sumneko_lua.setup({
	settings = {
		Lua = {
			workspace = {
				library = vim.api.nvim_get_runtime_file("", true),
			},
		},
	},
})

-- Set Icons
vim.diagnostic.config({
	virtual_text = true,
	signs = true,
	underline = true,
	update_in_insert = false,
	severity_sort = true,
})

local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

require('nvim-custom-diagnostic-highlight').setup {}
