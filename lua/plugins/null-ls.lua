local null_ls = require("null-ls")

null_ls.setup({
    sources = {
        -- Linters
        --null_ls.builtins.diagnostics.cmake_lint,
        null_ls.builtins.diagnostics.cpplint,
        null_ls.builtins.diagnostics.eslint_d,
        null_ls.builtins.diagnostics.markdownlint,
        null_ls.builtins.diagnostics.shellcheck,
        null_ls.builtins.diagnostics.luacheck,
        -- Formatters
        --null_ls.builtins.formatting.clang_format,
        --null_ls.builtins.formatting.cmake_format,
        null_ls.builtins.formatting.markdownlint,
        null_ls.builtins.formatting.prettier,
        null_ls.builtins.formatting.shfmt,
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.formatting.black
    },
})
