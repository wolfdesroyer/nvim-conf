---@diagnostic disable: undefined-global
require("impatient")
vim.g.pdf_convert_on_edit = 1
vim.g.pdf_convert_on_read = 1
vim.g.mapleader = " "
vim.keymap.set("n", "<Space>", "<Nop>", { silent = true, remap = false })
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.autoindent = true
vim.opt.number = true
vim.opt.smarttab = true
vim.opt.smartindent = true
vim.opt.ignorecase = true
vim.opt.mouse = "a"
vim.opt.termguicolors = true
vim.opt.completeopt = "menu,menuone,noselect"
vim.opt.signcolumn = "yes"
vim.opt.autochdir = true
vim.opt.cursorline = true
vim.opt.mousemodel = extend

vim.cmd([[highlight CursorLineNr guibg=#3b4252]])
vim.cmd([[highlight CursorLine guibg=#3b4252]])
vim.cmd([[set list listchars=tab:>\ ,trail:-,nbsp:+,eol:]])
vim.cmd([[set winbar=%m%t]])
vim.cmd([[set clipboard+=unnamedplus]])

require("plugins")
require("keybinds")
require("autocommands")
require("plugins-loader")
